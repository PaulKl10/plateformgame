import { Grid } from "./Grid";
import { Player } from "./Player/Player";

export class Game {
    grid: Grid
    players: Player[];
    whoPlay: number = 0;
    tour: number = 0;
    healKitPop: number = 0;
    thunderPop: number = 0;

    constructor(grid: Grid, players: Player[]) {
        this.grid = grid;
        this.players = players;
        this.healKitPop = Math.floor(Math.random() * (10 - 1) + 1);
        this.thunderPop = Math.floor(Math.random() * (30 - 5) + 5);
    }

    public showValidAdjacentCells() {
        if (this.whoPlay === 0) {
            const ValidAdjacentCells = this.players[0].validAdjacentCells[this.players[0].position];
            for (const cell of ValidAdjacentCells) {
                let cellule = (document.querySelector(`#${cell}`) as HTMLDivElement);
                if (cellule) {
                    cellule.classList.add("valid");
                }
            }
        } else {
            const ValidAdjacentCells = this.players[1].validAdjacentCells[this.players[1].position];
            for (const cell of ValidAdjacentCells) {
                let cellule = (document.querySelector(`#${cell}`) as HTMLDivElement);
                if (cellule) {
                    cellule.classList.add("valid");
                }
            }
        }
    }

    public resetCells() {
        const cells = document.querySelectorAll('.cell');
        for (const cell of cells) {
            if (cell.classList.contains("valid")) {
                cell.classList.remove("valid");
            }
        }
    }

    public resetItemCell(unit: string) {
        const cells = document.querySelectorAll('.cell');
        for (const cell of cells) {
            if (cell.classList.contains('healKit') && unit === cell.id) {
                cell.querySelector('img')?.remove();
                cell.classList.remove('healKit');
            } else if (cell.classList.contains('thunder') && unit === cell.id) {
                cell.querySelector('img')?.remove();
                cell.classList.remove('thunder');
            }
        }
    }

    public movePlayer(unit: string) {
        this.resetCells();
        let player!: Player;
        if (this.whoPlay === 0) {
            player = this.players[0];
        } else {
            player = this.players[1];
        }
        if (player.validAdjacentCells[player.position].includes(unit)) {
            if (this.whoPlay === 0) {
                this.whoPlay = 1;
            } else {
                this.whoPlay = 0;
            }
            const positionPlayer = (document.querySelector(`#${player.position}`) as HTMLDivElement);
            if (positionPlayer) {
                positionPlayer.innerText = "";
                positionPlayer.classList.remove(player.name);
                positionPlayer.removeChild;
            }
            const unitClicked = (document.querySelector(`#${unit}`) as HTMLDivElement);
            if (unitClicked) {
                const text = document.createElement('span');
                text.textContent = player.name;
                text.style.pointerEvents = "none";
                const image = document.createElement('img');
                image.setAttribute("src", "./assets/" + player.image);
                player.position = unit;
                const playerLife = document.createElement('div');
                playerLife.classList.add("progress");
                playerLife.setAttribute('role', 'progressbar');
                playerLife.setAttribute('aria-label', 'Basic exemple');
                playerLife.setAttribute('aria-valuenow', `${player.life}`);
                playerLife.setAttribute('aria-valuemin', '0');
                playerLife.setAttribute('aria-valuemax', '100');
                playerLife.style.backgroundColor = 'black';
                const playerLifeBar = document.createElement('div');
                playerLifeBar.classList.add("progress-bar");
                playerLifeBar.style.width = `${player.life}%`;
                playerLifeBar.style.backgroundColor = `red`;
                playerLife.appendChild(playerLifeBar);
                unitClicked.classList.add(player.name);
                unitClicked.appendChild(text);
                unitClicked.appendChild(image);
                unitClicked.appendChild(playerLife);
                const actions = (document.querySelector('.actions') as HTMLDivElement);
                let span = document.createElement('span');
                if (actions) {
                    if (player === this.players[0]) {
                        span.style.color = "yellow";
                    } else if (player === this.players[1]) {
                        span.style.color = "red";
                    }
                    span.textContent += player.name + " s'est déplacé en " + unit;
                    actions.append(span);
                }
                this.tour++;
                this.isHealKitCanPop();
                this.isThunderCanPop();
            }
        }
        this.showValidAdjacentCells();
    }

    public hitPlayer(clickedButton: HTMLDivElement) {
        this.resetCells();
        let player!: Player;
        let player2!: Player;
        let degat: number = 0;
        if (this.whoPlay === 0 && clickedButton.classList.contains(this.players[1].name)) {
            player = this.players[1];
            player2 = this.players[0];
        } else if (this.whoPlay === 1 && clickedButton.classList.contains(this.players[0].name)) {
            player = this.players[0];
            player2 = this.players[1];
        } else {
            return;
        }
        if (player2.validAdjacentCells[player2.position].includes(player.position) && clickedButton.id === player.position) {
            if (this.whoPlay === 0) {
                this.whoPlay = 1;
            } else {
                this.whoPlay = 0;
            }
            if (player2.role === "guerrier") {
                degat = Math.floor(Math.random() * (50 - 15) + 15);
                player.life -= degat;
            } else if (player2.role === "sorciere") {
                console.log("Sorcière frappe");
                player.life -= Math.floor(Math.random() * 30);
            }
            const positionPlayer = (document.querySelector(`#${player.position}`) as HTMLDivElement);
            const progressBar = positionPlayer.querySelector('div');
            if (progressBar) {
                progressBar.setAttribute('aria-valuenow', `${player.life}`);
                const progress = progressBar.querySelector('div');
                if (progress) {
                    progress.style.width = `${player.life}%`;
                }
            }
            const actions = (document.querySelector('.actions') as HTMLDivElement);
            let degatSubit = document.createElement('span');
            let span = document.createElement('span');
            if (actions) {
                degatSubit.style.color = 'orange';
                if (player === this.players[0]) {
                    span.style.color = "yellow";
                } else if (player === this.players[1]) {
                    span.style.color = "red";
                }
                span.textContent = player2.name + " a attaqué " + player.name;
                degatSubit.textContent = player.name + " a subit " + degat + " dégats";
                actions.append(span);
                actions.append(degatSubit);
            }
            this.isPlayerDied();
            this.tour++;
            this.isHealKitCanPop();
            this.isThunderCanPop();
        }
        this.showValidAdjacentCells();
    }

    public catchHealKit(unit: string) {
        let player!: Player;
        if (this.whoPlay === 0) {
            player = this.players[0];
        } else {
            player = this.players[1];
        }
        player.life = 100;
        this.resetItemCell(unit);
        this.movePlayer(unit);
    }

    public catchThunder(unit: string) {
        console.log('coucou');
        let player!: Player;
        if (this.whoPlay === 0) {
            player = this.players[0];
        } else {
            player = this.players[1];
        }
        for (let row = 0; row < 5; row++) {
            for (let col = 0; col < 8; col++) {
                const cellId = `cell_${row}_${col}`;
                player.validAdjacentCells[cellId] = [];

                for (let i = -4; i <= 4; i++) {
                    for (let j = -4; j <= 4; j++) {
                        if (i === 0 && j === 0) {
                            continue;
                        }
                        const newRow = row + i;
                        const newCol = col + j;

                        if (newRow >= 0 && newRow < 5 && newCol >= 0 && newCol < 8) {
                            const adjacentCellId = `cell_${newRow}_${newCol}`;
                            player.validAdjacentCells[cellId].push(adjacentCellId);
                        }
                    }
                }
            }
        }
        this.resetItemCell(unit);
        this.movePlayer(unit);
    }

    public isHealKitCanPop() {
        console.log(this.tour);
        console.log("HealKit : " + this.healKitPop);
        if (this.tour === this.healKitPop) {
            this.grid.generateHealKit();
            this.healKitPop = this.tour + Math.floor(Math.random() * (this.tour - 4) + 4);
        }
    }

    public isThunderCanPop() {
        console.log("thunderPop : " + this.thunderPop);
        if (this.tour === this.thunderPop) {
            this.grid.generateThunder();
            this.thunderPop = this.tour + Math.floor(Math.random() * ((this.tour + 10) - 4) + 4);
        }
    }

    public isPlayerDied() {
        let count = 0;
        for (const player of this.players) {
            if (player.life <= 0) {
                const positionPlayer = (document.querySelector(`#${player.position}`) as HTMLDivElement);
                if (positionPlayer) {
                    positionPlayer.innerText = "";
                    positionPlayer.removeChild;
                }
                this.players.splice(count, 1);
                document.body.innerHTML = "";
                let finish = document.createElement('section');
                finish.style.backgroundColor = "gray";
                finish.innerHTML += `<h1 class="text-center text-warning">${this.players[0].name} a gagné !</h1>`;
                let button = document.createElement('button');
                button.textContent = "Rejouer";
                button.classList.add('rejouer');
                button.style.display = "block";
                button.style.width = "auto"
                button.style.margin = 'auto';
                button.style.padding = "5px";
                finish.append(button);
                document.body.append(finish);
                button.addEventListener('click', () => {
                    location.reload();
                })
                this.resetCells();
            }
            count++;
        }
    }
}