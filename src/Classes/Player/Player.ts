export class Player {
    name: string;
    role!: string;
    life: number;
    mana: number;
    image!: string;
    position: string = "";
    validAdjacentCells: { [cellId: string]: string[] } = {};


    constructor(name: string, life: number, mana: number) {
        this.name = name;
        this.life = life;
        this.mana = mana;
    }


}