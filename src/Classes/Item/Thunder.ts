import { Item } from "./Item";

export class Thunder extends Item {

    constructor() {
        super();
        this.name = "Thunder";
        this.image = "thunder.png";
    }
}