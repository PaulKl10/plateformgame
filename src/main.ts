import { Game } from './Classes/Game';
import { Grid } from './Classes/Grid';
import { Guerrier } from './Classes/Player/Guerrier';
import { Player } from './Classes/Player/Player';
import { Sorcier } from './Classes/Player/Sorcier';

const start = document.querySelector('#start');
if (start) {
  start.addEventListener('click', () => {
    const player1Name: string = (document.querySelector("#player1Name") as HTMLInputElement).value;
    const player2Name: string = (document.querySelector("#player2Name") as HTMLInputElement).value;
    const player1Role: string = (document.querySelector("#player1Role") as HTMLSelectElement).value;
    const player2Role: string = (document.querySelector("#player2Role") as HTMLSelectElement).value;
    let player1!: Player;
    let player2!: Player;
    switch (player1Role) {
      case "Guerrier":
        player1 = new Guerrier(player1Name, 100, 100);
        break;
      case "Sorciere":
        player1 = new Sorcier(player1Name, 100, 150);
        break;

      default:
        break;
    }
    switch (player2Role) {
      case "Guerrier":
        player2 = new Guerrier(player2Name, 100, 100);
        break;
      case "Sorciere":
        player2 = new Sorcier(player2Name, 100, 150);
        break;

      default:
        break;
    }
    const grid = new Grid(5, 8, [player1, player2]);
    const game = new Game(grid, [player1, player2]);
    grid.generateGridWithPlayers();
    game.showValidAdjacentCells();
    const buttons = document.querySelectorAll(".cell");
    for (const button of buttons) {
      button.addEventListener('click', (e) => {
        const clickedButton = e.target as HTMLDivElement;
        const isPlayer = clickedButton.querySelector('span');
        const isHealKit = button.classList.contains('healKit');
        const isThunder = button.classList.contains('thunder');
        const buttonId = clickedButton.id;
        if (isPlayer) {
          game.hitPlayer(clickedButton);
        } else if (isHealKit) {
          game.catchHealKit(buttonId);
        } else if (isThunder) {
          game.catchThunder(buttonId);
        } else {
          game.movePlayer(buttonId);
        }
      })
    }

  })
}
