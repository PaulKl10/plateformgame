import { HealKit } from "./Item/HealKit";
import { Thunder } from "./Item/Thunder";
import { Player } from "./Player/Player";

export class Grid {
    rows: number;
    cols: number;
    players: Player[];

    constructor(rows: number, cols: number, players: Player[]) {
        this.rows = rows;
        this.cols = cols;
        this.players = players;
    }

    public generateGridWithPlayers() {
        const container = document.createElement('section');
        const plateau = document.createElement('div');
        const actions = document.createElement('div');
        actions.classList.add('actions');
        container.append(plateau);
        container.append(actions);
        container.style.display = "flex";
        container.style.width = "100%"
        container.style.height = "auto";
        container.style.overflow = "hidden";
        document.body.innerHTML = "";
        document.body.appendChild(container);

        let randomRow = Math.floor(Math.random() * this.rows);
        let randomCol = Math.floor(Math.random() * this.cols);
        this.players[0].position = `cell_${randomRow}_${randomCol}`;

        let random2Row = Math.floor(Math.random() * this.rows);
        let random2Col = Math.floor(Math.random() * this.cols);
        while (randomRow === random2Row && randomCol === random2Col) {
            random2Row = Math.floor(Math.random() * this.rows);
            random2Col = Math.floor(Math.random() * this.cols);
        }
        this.players[1].position = `cell_${random2Row}_${random2Col}`;

        // Style css de la grid
        plateau.style.display = 'grid';
        plateau.style.gridTemplateColumns = `repeat(${this.cols}, 100px)`;
        plateau.style.gridTemplateRows = `repeat(${this.rows}, 100px)`;
        plateau.style.gap = '20px';
        plateau.style.justifyContent = "start";
        plateau.style.marginTop = "20px";

        for (let i = 0; i < this.rows; i++) {
            for (let j = 0; j < this.cols; j++) {
                const unit = document.createElement('div');
                unit.classList.add('cell');
                const uniqueId = `cell_${i}_${j}`;
                unit.setAttribute('id', uniqueId);

                for (const player of this.players) {
                    if (uniqueId === player.position) {
                        const text = document.createElement('span');
                        text.textContent = player.name;
                        text.style.pointerEvents = "none";
                        unit.appendChild(text);
                        unit.classList.add(player.name);
                        const image = document.createElement('img');
                        image.setAttribute("src", "./assets/" + player.image);
                        unit.appendChild(image);
                        const playerLife = document.createElement('div');
                        playerLife.classList.add("progress");
                        playerLife.setAttribute('role', 'progressbar');
                        playerLife.setAttribute('aria-label', 'Basic exemple');
                        playerLife.setAttribute('aria-valuenow', `${player.life}`);
                        playerLife.setAttribute('aria-valuemin', '0');
                        playerLife.setAttribute('aria-valuemax', '100');
                        playerLife.style.backgroundColor = 'black';
                        const playerLifeBar = document.createElement('div');
                        playerLifeBar.classList.add("progress-bar");
                        playerLifeBar.style.width = `${player.life}%`;
                        playerLifeBar.style.backgroundColor = `red`;
                        playerLife.appendChild(playerLifeBar);
                        unit.appendChild(playerLife);
                    }
                }
                plateau.appendChild(unit);
            }
        }
    }

    public generateHealKit() {
        let healKit = new HealKit();
        let randomRow = Math.floor(Math.random() * this.rows);
        let randomCol = Math.floor(Math.random() * this.cols);
        healKit.position = `cell_${randomRow}_${randomCol}`;
        while (healKit.position === this.players[0].position || healKit.position === this.players[1].position) {
            console.log("yes");
            randomRow = Math.floor(Math.random() * this.rows);
            randomCol = Math.floor(Math.random() * this.cols);
            healKit.position = `cell_${randomRow}_${randomCol}`;
        }
        let cells = document.querySelectorAll('.cell');
        for (const cell of cells) {
            if (cell.id === healKit.position) {
                let image = document.createElement('img');
                image.setAttribute("src", "./assets/" + healKit.image);
                cell.appendChild(image);
                cell.classList.add("healKit");
            }
        }
    }

    public generateThunder() {
        let thunder = new Thunder();
        let randomRow = Math.floor(Math.random() * this.rows);
        let randomCol = Math.floor(Math.random() * this.cols);
        thunder.position = `cell_${randomRow}_${randomCol}`;
        while (thunder.position === this.players[0].position || thunder.position === this.players[1].position) {
            console.log("yes");
            randomRow = Math.floor(Math.random() * this.rows);
            randomCol = Math.floor(Math.random() * this.cols);
            thunder.position = `cell_${randomRow}_${randomCol}`;
        }
        let cells = document.querySelectorAll('.cell');
        for (const cell of cells) {
            if (cell.id === thunder.position) {
                let image = document.createElement('img');
                image.setAttribute("src", "./assets/" + thunder.image);
                cell.appendChild(image);
                cell.classList.add("thunder");
            }
        }
    }
}