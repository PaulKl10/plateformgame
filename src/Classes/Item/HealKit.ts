import { Item } from "./Item";

export class HealKit extends Item {
    heal: number;

    constructor() {
        super();
        this.name = "Heal Kit";
        this.heal = 100;
        this.image = "healKit.png";
    }
}