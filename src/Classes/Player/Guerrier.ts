import { Player } from "./Player";

export class Guerrier extends Player {

    constructor(name: string, life: number, mana: number) {
        super(name, life, mana);
        this.image = "Guerrier.webp";
        this.role = "guerrier";
        this.generateValidAdjacentCells();
    }

    private generateValidAdjacentCells() {
        for (let row = 0; row < 5; row++) {
            for (let col = 0; col < 8; col++) {
                const cellId = `cell_${row}_${col}`;
                this.validAdjacentCells[cellId] = [];

                for (let i = -1; i <= 1; i++) {
                    for (let j = -1; j <= 1; j++) {
                        if (i === 0 && j === 0) {
                            continue;
                        }
                        const newRow = row + i;
                        const newCol = col + j;

                        if (newRow >= 0 && newRow < 5 && newCol >= 0 && newCol < 8) {
                            const adjacentCellId = `cell_${newRow}_${newCol}`;
                            this.validAdjacentCells[cellId].push(adjacentCellId);
                        }
                    }
                }
            }
        }
    }

}